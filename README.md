# Git tips

## Aliases

* generique: `git config (--global) alias.<shortcut> 'cmd'`

```bash
git config --global alias.s 'status'
git confing --global alias.unstage 'reset HEAD'
git confing --global alias.logv 'log --graph --all --reflog'
git confing --global alias.l 'log --graph --oneline --decorate'
```

## Astuce

* Pour afficher l'historique du `HEAD`:  `git reflog`
* Pour afficher tous les commits (même orphelins): `git log --all --graph --reflog`
* `git checkout HEAD~2`: revenir sur 2 commit avant le HEAD actuel
* `git checkout HEAD^`: se déplace sur le parent = `git checkout HEAD~1`
* `git reset --hard HEAD^`: Revient sur commit précedent
    * `git reset --hard ORIG_HEAD`: Si le `HEAD^` est indispo car orphelin, cette commande permet quand même de revenir dessus.
    * `--hard`: Supprime ce qui est non indexé et ce qui est indexé et non commit
    * `--mixed`: Supprime uniquement ce qui est indexé.
    * `--soft`: Conserve les modif locales et indexées
    * `git reset HEAD .`: permet de désindexé les modifs
* `git init --bare <repo_name>`: Permet de ne pas configurer d'espace de travail: on ne peut pas travailler directement dans le repo. Utile pour une utilisation en mode serveur, dans laquelle le repo est uniquement là pour servir de reference.
* Pour diff des modif dans la zone de stage: `git diff --cached <mon-fichier>`
* Pour faire de la recherche par dichotomie: `git bisect`

* `git rebase -i HEAD~<N>` (avec N=nombre commit avant le HEAD) Pour tout ce qui est modification de commit du type:
    * renommer un commit (Noter que le `git commit --amend` ne permet de modifier que le commit précédent)
    * squash les commit (eg fusionner)
    * spliter des commit 
    * supprimer un commit
    * . . . 

## Use case

* J'ai merge master sur develop et non l'inverse:
  * Retrouver le commit d'avant le merge:
    * `git log --graph` pour retrouver le commit
    * `git log --all --reflog --graph` si la branch a été supprimée pendant les merge et que les commit ne sont plus référencés
  * Faire un reset sur le commit d'avant le merge
    * `git reset --hard <sha1_commmit>` 
    OU 
    * `git reset --hard ORIG_HEAD`: Cela évite d'aller récupérer l'ancien commit, car le ORIG_HEAD pointe sur le HEAD précedent
  * À ce niveau, nous sommes dans la situation avant le merge:
    1. Se placer sur la branche master: `git checkout master`
    2. Faire le merge correctement: `git merge develop`
    3. Regler les conflits éventuels
        * éditer les fichiers qui posent problèmes
        * `git add <fichiers_conflits>`
        * `git merge --continue`

* Rebase de la branche master sur develop au lieu de develop sur master
    * `git reset --hard ORIG_HEAD`
    * `git checkout develop`
    * `git rebase master`
        * Régler les conflits éventuels
        * `vim` sur les fichiers
        * `git add .`
        * `git rebase --continue`

## Pour gérer les alias

* generique: `git config (--global) alias.<shortcut> 'cmd'`
* `git config --global alias.s 'status'`
* `git confing --global alias.unstage 'reset HEAD'`
* `git confing --global alias.logv 'log --graph --all --reflog'`
* `git confing --global alias.l 'log --graph --oneline --decorate'`

## Submodules

* Ajouter un submodule: `git submodule add`

* Lorsqu'on clone une super depot (== un depot qui contient des submodule). Il faut configurer les submodules.
    * Soit avec:
        ```bash
        git clone super-repo
        git submodule init
        git submodule update
        ```
    * Soit direcement lors du clone: 
        ```bash
        git clone --recurse-submodules
        ```

* Pour que le super depot pousse également les modifications du submodule si modification depuis le super depot: `git push --recurse-submodules=check`

